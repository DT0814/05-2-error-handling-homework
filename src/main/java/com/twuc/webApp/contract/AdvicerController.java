package com.twuc.webApp.contract;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

/**
 * @author tao.dong
 */
@ControllerAdvice
public class AdvicerController extends ResponseEntityExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(AdvicerController.class);

    @ExceptionHandler(value
            = {IllegalArgumentException.class})
    protected ResponseEntity<Message> handleConflict(IllegalArgumentException exception) {
        logger.error("error={},time={}",exception.getMessage(),LocalDateTime.now());
        return ResponseEntity
                .status(HttpStatus.EXPECTATION_FAILED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new Message(exception.getMessage()));
    }

    class Message {
        private String message;

        public Message(String message) {
            this.message = message;
        }

        public Message() {
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}
